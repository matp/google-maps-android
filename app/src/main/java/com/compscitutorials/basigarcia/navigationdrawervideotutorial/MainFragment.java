package com.compscitutorials.basigarcia.navigationdrawervideotutorial;


import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SearchView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.vision.barcode.Barcode;

import java.io.IOException;
import java.util.List;

public class MainFragment extends Fragment implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,View.OnClickListener,SearchView.OnQueryTextListener {





    Geocoder coder;

    Barcode.GeoPoint p;
    private double latitude;
    private double longitude;
    MapView mapView;
    Marker marker;
    GoogleMap map;
    Location mLastLocation;
    GoogleApiClient mGoogleApiClient;


    Location locate;

    private Button btnShowLocation;


    private double Latitude=0;

    private double Longitude=0;




    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // What i have added is this

        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu (Menu menu, MenuInflater inflater) {

        //inflater.inflate(R.menu.main, menu); // removed to not double the menu items
        MenuItem item = menu.findItem(R.id.action_search);
        SearchView sv = new SearchView(((MainActivity) getActivity()).getSupportActionBar().getThemedContext());
        MenuItemCompat.setShowAsAction(item, MenuItemCompat.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW | MenuItemCompat.SHOW_AS_ACTION_IF_ROOM);
        MenuItemCompat.setActionView(item, sv);
        sv.setOnQueryTextListener(this);
        sv.setIconifiedByDefault(false);
        sv.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.w("QUERY", "Clicked: " + view);
            }
        });

        MenuItemCompat.setOnActionExpandListener(item, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                // Do something when collapsed
                //    Utils.LogDebug("Closed: ");
                Log.w("QUERY", "Closed: " + item);
                return true;  // Return true to collapse action view
            }

            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                // Do something when expanded
                //  Utils.LogDebug("Openeed: ");
                Log.w("QUERY", "Opened: " + item);
                return true;  // Return true to expand action view
            }
        });

        super.onCreateOptionsMenu(menu, inflater);
    }











    @Override
    public boolean onQueryTextSubmit(String query) {
        Log.w("QUERY", "Submitted: " + query);


        Geocoder gc = new Geocoder(getActivity());
        List<Address> list = null;


        try {

            list = gc.getFromLocationName(query, 1);


            Address add = list.get(0);
            String locality = add.getLocality();

            Toast.makeText(getActivity(), locality, Toast.LENGTH_SHORT).show();



            this.latitude = add.getLatitude();


            this.longitude = add.getLongitude();

            displayLocation(longitude, latitude);


        } catch (IOException e) {
            Toast.makeText(getActivity(), "noResultsFound", Toast.LENGTH_SHORT).show();
            Log.w("QUERY", "ERROR");

        } finally {
            return true;
        }


    }



    @Override
    public boolean onQueryTextChange(String newText) {
        Log.w("QUERY", "Changed: " + newText);








        return false;
    }

    private void displayLocation(double longitude,double latitude) {

        marker.remove();

        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 20);
        map.animateCamera(cameraUpdate);

        marker=map.addMarker(new MarkerOptions()
                .position(new LatLng(latitude, longitude)));
        marker.setVisible(true);


        Toast.makeText(getActivity(), latitude + ", " + longitude, Toast.LENGTH_SHORT).show();


    }



    private void displayLocation() {

        marker.remove();


        mLastLocation = LocationServices.FusedLocationApi
                .getLastLocation(mGoogleApiClient);

        if (mLastLocation != null) {
            double latitude = mLastLocation.getLatitude();
            double longitude = mLastLocation.getLongitude();


            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 20);
            map.animateCamera(cameraUpdate);

            marker=map.addMarker(new MarkerOptions()
                    .position(new LatLng(latitude, longitude)));
            marker.setVisible(true);




            Toast.makeText(getActivity(), latitude + ", " + longitude, Toast.LENGTH_SHORT).show();


        } else {

            Toast.makeText(getActivity(), "(Couldn't get your localization. Make sure GPS is enabled on the device)", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View v = inflater.inflate(R.layout.fragment_main, container, false);
        // Gets the MapView from the XML layout and creates it

        MapsInitializer.initialize(getActivity());

        btnShowLocation = (Button) v.findViewById(R.id.btnShowLocation);
        btnShowLocation.setOnClickListener(this);

        //   btnSearch = (Button) v.findViewById(R.id.action_search);
        //  btnSearch.setOnClickListener(this);


        switch (GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity())) {
            case ConnectionResult.SUCCESS:
                Toast.makeText(getActivity(), "SUCCESS", Toast.LENGTH_SHORT).show();
                mapView = (MapView) v.findViewById(R.id.map);
                mapView.onCreate(savedInstanceState);
                // Gets to GoogleMap from the MapView and does initialization stuff
                if (mapView != null) {
                    map = mapView.getMap();
                    map.getUiSettings().setMyLocationButtonEnabled(false);
                    map.getUiSettings().setZoomControlsEnabled(true);
                    map.setMyLocationEnabled(true);

                }
                break;
            case ConnectionResult.SERVICE_MISSING:
                Toast.makeText(getActivity(), "SERVICE MISSING", Toast.LENGTH_SHORT).show();
                break;
            case ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED:
                Toast.makeText(getActivity(), "UPDATE REQUIRED", Toast.LENGTH_SHORT).show();
                break;
            default:
                Toast.makeText(getActivity(), GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity()), Toast.LENGTH_SHORT).show();
        }

        // Updates the location and zoom of the MapView

        // LatLng sydney = new LatLng(-33.867, 151.206);

        //   map.setMyLocationEnabled(true);
        // map.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 13));

        // map.addMarker(new MarkerOptions()
        //      .title("Sydney")
        //      .snippet("The most populous city in Australia.")
        //    .position(sydney));


        //  map.addMarker(new MarkerOptions()
        //        .position(new LatLng(10, 10))
        //      .title("Hello worl"));


        return v;

    }


    public void onViewCreated(View v, Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);




        //  mGoogleApiClient = new GoogleApiClient
        //          .Builder(getActivity())
        //          .addApi(Places.GEO_DATA_API)
        //          .addApi(Places.PLACE_DETECTION_API)
        //          .addConnectionCallbacks((GoogleApiClient.ConnectionCallbacks) this)
        //          .build();
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();


        marker = map.addMarker(new MarkerOptions()
                .position(new LatLng(100, 100)));


        marker.setVisible(false);


    }


    @Override
    public void onResume() {
        mapView.onResume();

        super.onResume();


    }


    @Override
    public void onPause() {

        super.onPause();


    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();

        mapView.onLowMemory();
    }


    @Override
    public void onStart() {
        super.onStart();


        Log.w("CONNECTION", "onStartmethod started");

        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
            Log.w("CONNECTION", "Connected with mGoogleAPiClient");


        }

        if (mGoogleApiClient == null) {

            Log.w("CONNECTION", "Failed to connect with mGoogleAPIclient");

        }


    }


    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Log.w("CONNECTION", "Connection failed:" + result.getErrorMessage());

    }

    @Override
    public void onConnected(Bundle arg0) {

        Log.w("CONNECTION", "Connection success");


        displayLocation();


    }

    @Override
    public void onConnectionSuspended(int arg0) {
        mGoogleApiClient.connect();
    }


    @Override
    public void onClick(View v) {


        switch (v.getId()) {
            case R.id.btnShowLocation:
                Log.d("LOCATION", "Connection success");
                displayLocation();
                break;



        }
    }
}





